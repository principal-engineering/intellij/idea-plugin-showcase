package com.example.ideapluginshowcase.runconfigurations.showcase1

import com.intellij.execution.configurations.RunConfigurationOptions
import com.intellij.openapi.components.StoredProperty


class Showcase1RunConfigurationOptions : RunConfigurationOptions() {
  private val myScriptName: StoredProperty<String?> = string("").provideDelegate(this, "scriptName")
  private val myScriptArgs: StoredProperty<String?> = string("").provideDelegate(this, "scriptArgs")

  var scriptName: String?
    get() = myScriptName.getValue(this)
    set(scriptName) {
      myScriptName.setValue(this, scriptName)
    }

  var scriptArgs: String?
    get() = myScriptArgs.getValue(this)
    set(scriptArgs) {
      myScriptArgs.setValue(this, scriptArgs)
    }
}
