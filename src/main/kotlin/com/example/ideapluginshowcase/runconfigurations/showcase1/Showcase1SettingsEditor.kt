package com.example.ideapluginshowcase.runconfigurations.showcase1

import com.intellij.codeInsight.completion.CompletionResultSet
import com.intellij.openapi.externalSystem.service.execution.cmd.CommandLineCompletionProvider
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory
import com.intellij.openapi.options.SettingsEditor
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.TextFieldWithBrowseButton
import com.intellij.util.textCompletion.TextFieldWithCompletion
import com.intellij.util.ui.FormBuilder
import org.apache.commons.cli.Options
import org.jetbrains.annotations.NotNull
import javax.swing.JComponent
import javax.swing.JPanel


class Showcase1SettingsEditor(project: Project) : SettingsEditor<Showcase1RunConfiguration>() {
  private val myPanel: JPanel
  private val scriptPathField: TextFieldWithBrowseButton
  private val scriptArgsField: TextFieldWithCompletion

  private val myOptions: Options = Options()
    .addOption("f", "foo", false, "...")
    .addOption("b", "bar", false, "...")
    .addOption("q", "quix", false, "...")

  init {
    scriptPathField = TextFieldWithBrowseButton()
    scriptPathField.addBrowseFolderListener(
      "Select Script File", null, null,
      FileChooserDescriptorFactory.createSingleFileDescriptor()
    )

    scriptArgsField = TextFieldWithCompletion(project, Showcase1CompletionProvider(myOptions), "", true, true, true)

    myPanel = FormBuilder.createFormBuilder()
      .addLabeledComponent("Script file", scriptPathField)
      .addLabeledComponent("Script args", scriptArgsField)
      .panel
  }

  override fun resetEditorFrom(showcase1RunConfiguration: Showcase1RunConfiguration) {
    scriptPathField.setText(showcase1RunConfiguration.scriptName)
    scriptArgsField.setText(showcase1RunConfiguration.scriptArgs)
  }

  override fun applyEditorTo(@NotNull showcase1RunConfiguration: Showcase1RunConfiguration) {
    showcase1RunConfiguration.scriptName = scriptPathField.text
    showcase1RunConfiguration.scriptArgs = scriptArgsField.text
  }

  @NotNull
  override fun createEditor(): JComponent {
    return myPanel
  }
}

class Showcase1CompletionProvider(options: Options) : CommandLineCompletionProvider(options) {

  override fun addArgumentVariants(result: CompletionResultSet) {
  }
}
