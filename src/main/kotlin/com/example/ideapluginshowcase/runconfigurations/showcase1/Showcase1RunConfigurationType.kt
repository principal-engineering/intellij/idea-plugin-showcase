package com.example.ideapluginshowcase.runconfigurations.showcase1

import com.intellij.execution.configurations.ConfigurationTypeBase
import com.intellij.icons.AllIcons
import com.intellij.openapi.util.NotNullLazyValue


class Showcase1RunConfigurationType protected constructor() :
  ConfigurationTypeBase(
    ID, "Showcase1", "Showcase1 run configuration type",
    NotNullLazyValue.createValue { AllIcons.Nodes.Console }) {
  init {
    addFactory(Showcase1ConfigurationFactory(this))
  }

  companion object {
    const val ID = "Showcase1RunConfiguration"
  }
}
