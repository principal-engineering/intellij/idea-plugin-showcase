package com.example.ideapluginshowcase.runconfigurations.showcase1

import com.intellij.execution.Executor
import com.intellij.execution.configurations.*
import com.intellij.execution.process.ProcessHandler
import com.intellij.execution.process.ProcessHandlerFactory
import com.intellij.execution.process.ProcessTerminatedListener
import com.intellij.execution.runners.ExecutionEnvironment
import com.intellij.openapi.options.SettingsEditor
import com.intellij.openapi.project.Project
import com.jetbrains.rd.util.ExecutionException
import org.jetbrains.annotations.NotNull


class Showcase1RunConfiguration(
  project: Project?,
  factory: ConfigurationFactory?,
  name: String?
) :
  RunConfigurationBase<Showcase1RunConfigurationOptions?>(project!!, factory, name) {
  @NotNull
  override fun getOptions(): Showcase1RunConfigurationOptions {
    return super.getOptions() as Showcase1RunConfigurationOptions
  }

  var scriptName: String?
    get() = options.scriptName
    set(scriptName) {
      options.scriptName = scriptName
    }

  var scriptArgs: String?
    get() = options.scriptArgs
    set(scriptArgs) {
      options.scriptArgs = scriptArgs
    }

  @NotNull
  override fun getConfigurationEditor(): SettingsEditor<out RunConfiguration?> {
    return Showcase1SettingsEditor(project)
  }

  override fun getState(executor: Executor, environment: ExecutionEnvironment): RunProfileState? {
    return object : CommandLineState(environment) {
      @NotNull
      @Throws(ExecutionException::class)
      override fun startProcess(): ProcessHandler {
        var opts = arrayListOf(options.scriptName)

        // Functional Idiomatic
        options.scriptArgs?.let { args ->
          args
            .split(" ")
            .filter { it.isNotBlank() && it.isNotEmpty() }
            .forEach { opts.add(it) }
        }

        // Imperative buggy
//        if (options.scriptArgs != null) {
//          val res = options.scriptArgs!!.split(" ")
//          var args = ArrayList<String>()
//
//          for (v in res) {
//            if (v.isNotBlank() && v.isNotEmpty()) {
//              args.add(v)
//            }
//          }
//        }

        val commandLine = GeneralCommandLine(opts.toList())
        val processHandler = ProcessHandlerFactory.getInstance()
          .createColoredProcessHandler(commandLine)
        ProcessTerminatedListener.attach(processHandler)
        return processHandler
      }
    }
  }
}
