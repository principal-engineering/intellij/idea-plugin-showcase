package com.example.ideapluginshowcase.runconfigurations.showcase1

import com.intellij.execution.configurations.ConfigurationFactory
import com.intellij.execution.configurations.ConfigurationType
import com.intellij.execution.configurations.RunConfiguration
import com.intellij.openapi.components.BaseState
import com.intellij.openapi.project.Project
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable


class Showcase1ConfigurationFactory(type: ConfigurationType?) : ConfigurationFactory(type!!) {
  override fun createTemplateConfiguration(project: Project): RunConfiguration {
    return Showcase1RunConfiguration(project, this, "Showcase 1")
  }

  @NotNull
  override fun getId(): String {
    return Showcase1RunConfigurationType.ID
  }

  @Nullable
  override fun getOptionsClass(): Class<out BaseState>? {
    return Showcase1RunConfigurationOptions::class.java
  }
}
