package com.example.ideapluginshowcase.actions

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent


class ShowcaseAction : AnAction() {
  override fun actionPerformed(e: AnActionEvent) {
    println("Hello, Action Showcase!")
  }
}
